package com.example.micandcamapp

import android.Manifest
import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.AccessibilityServiceInfo
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Camera
import android.hardware.camera2.CameraManager
import android.hardware.camera2.CameraManager.AvailabilityCallback
import android.media.AudioFormat
import android.media.AudioManager
import android.media.AudioRecord
import android.media.MediaRecorder
import android.os.*
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.accessibility.AccessibilityManager
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.lang.RuntimeException
import java.util.*


class MainActivity : AppCompatActivity() {
    private val MY_PERMISSIONS_RECORD_AUDIO = 1
    private val MY_PERMISSIONS_RECORD_CAM = 2
    private var mic: Button? = null
    private var camera: Button? = null
    var end: Long = 0
    var start: Long = 0
    var otherAppAudioTimer: Timer? = null
    var v: Vibrator? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mic = findViewById<View>(R.id.button_mic) as Button
        camera = findViewById<View>(R.id.button_camera) as Button
        mic!!.setOnClickListener { listener() }
        camera!!.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    this@MainActivity, Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this@MainActivity, arrayOf(Manifest.permission.CAMERA),
                    MY_PERMISSIONS_RECORD_CAM
                )
            } else {
                val manager =
                    getSystemService(CAMERA_SERVICE) as CameraManager
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    manager.registerAvailabilityCallback(object : AvailabilityCallback() {
                        override fun onCameraAvailable(cameraId: String) {
                            super.onCameraAvailable(cameraId)
                            Toast.makeText(this@MainActivity, "Cam is available", Toast.LENGTH_LONG)
                                .show()
                            end = System.currentTimeMillis()
                            val elapsedTime: Long = (((end - start) / 1000))
                            if (elapsedTime.toString().length < 6) Toast.makeText(
                                this@MainActivity,
                                "Cam was not available for $elapsedTime seconds", Toast.LENGTH_LONG
                            ).show()
                            return
                        }

                        override fun onCameraUnavailable(cameraId: String) {
                            super.onCameraUnavailable(cameraId)
                            Toast.makeText(
                                this@MainActivity,
                                "Cam is not available",
                                Toast.LENGTH_LONG
                            ).show()
                            start = System.currentTimeMillis()
                            // some time passes
                        }
                    }, null)
                }
            }
        }
    }

    public override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(
                this@MainActivity,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@MainActivity,
                arrayOf(Manifest.permission.RECORD_AUDIO),
                MY_PERMISSIONS_RECORD_AUDIO
            )
        } else {
            if (!isAccessibilityServiceEnabled(
                    this@MainActivity,
                    MyAccessibilityService::class.java
                )
            ) startActivity(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)) else {
                /*     val manager = this@MainActivity.getSystemService(AUDIO_SERVICE) as AudioManager
                     if (manager.mode == AudioManager.MODE_IN_CALL || manager.mode == AudioManager.MODE_IN_COMMUNICATION) {
                         Toast.makeText(this@MainActivity, "Mic is not available", Toast.LENGTH_LONG)
                             .show()
                     } else {
                         Toast.makeText(this@MainActivity, "Mic is available", Toast.LENGTH_LONG).show()
                     }*/
            }
        }
    }

    //onPause() unregister the accelerometer for stop listening the events
    override fun onPause() {
        super.onPause()
    }

    public override fun onStop() {
        super.onStop()
    }

    val isCameraAvailable: Boolean
        get() {
            var available = true
            var camera: Camera? = null
            try {
                camera = Camera.open()
            } catch (e: RuntimeException) {
                available = false
                return available
            } finally {
                camera?.release()
            }
            return available
        }

    private fun validateMicAvailability(): Boolean {
        var available = true
        @SuppressLint("MissingPermission") var recorder: AudioRecord? = AudioRecord(
            MediaRecorder.AudioSource.MIC, 44100,
            AudioFormat.CHANNEL_IN_MONO,
            AudioFormat.ENCODING_DEFAULT, 44100
        )
        try {
            if (recorder!!.recordingState != AudioRecord.RECORDSTATE_STOPPED) {
                available = false
            }
            recorder.startRecording()
            if (recorder.recordingState != AudioRecord.RECORDSTATE_RECORDING) {
                recorder.stop()
                available = false
            }
            recorder.stop()
        } finally {
            recorder!!.release()
            recorder = null
        }
        return available
    }

    fun listener() {
        val audioManager = getSystemService(AUDIO_SERVICE) as AudioManager
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            val mode = audioManager.mode

            // use AudioManager.MODE_IN_CALL for audio call
            // use AudioManager.MODE_IN_COMMUNICATION for audio/video chat or VOIP
            if (AudioManager.MODE_IN_COMMUNICATION == mode) {
                // Enters here during active internet call.
                Log.e("TAG", "In Internet Call (VOIP)")
                Toast.makeText(this@MainActivity, "Mic is not available", Toast.LENGTH_LONG).show()
            } else if (AudioManager.MODE_IN_CALL == mode) {
                // Enters here during active call.
                Log.e("TAG", "In Normal Call")
                Toast.makeText(this@MainActivity, "Mic is not available", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this@MainActivity, "Mic is available", Toast.LENGTH_LONG).show()
            }
        }, 1000)
    }

    //Handling callback
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MY_PERMISSIONS_RECORD_AUDIO -> {
                if (grantResults.size > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    // permission was granted, yay!
                    // validateMicAvailability();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permissions Denied to record audio", Toast.LENGTH_LONG)
                        .show()
                }
                return
            }
            MY_PERMISSIONS_RECORD_CAM -> {
                if (grantResults.size > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    // permission was granted, yay!
                    // validateMicAvailability();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permissions Denied to access cam", Toast.LENGTH_LONG)
                        .show()
                }
                return
            }
        }
    }

    companion object {
        fun isAccessibilityServiceEnabled(
            context: Context,
            service: Class<out AccessibilityService?>
        ): Boolean {
            val am = context.getSystemService(ACCESSIBILITY_SERVICE) as AccessibilityManager
            val enabledServices =
                am.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_ALL_MASK)
            for (enabledService in enabledServices) {
                val enabledServiceInfo = enabledService.resolveInfo.serviceInfo
                if (enabledServiceInfo.packageName == context.packageName && enabledServiceInfo.name == service.name) return true
            }
            return false
        }
    }
}