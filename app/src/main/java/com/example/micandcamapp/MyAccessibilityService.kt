package com.example.micandcamapp

import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.AccessibilityServiceInfo
import android.app.ActivityManager
import android.app.ActivityManager.RunningTaskInfo
import android.os.Build
import android.util.Log
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.Toast
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class MyAccessibilityService : AccessibilityService() {
    override fun onAccessibilityEvent(event: AccessibilityEvent) {
        Log.d("myaccess", "after lock")
        if (event.eventType == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED) {
            Log.d("myaccess", "in window changed")
            val info = event.source
            if (info != null && info.text != null) {
                val duration = info.text.toString()
                val zeroSeconds = String.format(
                    "%02d:%02d",
                    *arrayOf<Any>(Integer.valueOf(0), Integer.valueOf(0))
                )
                val firstSecond = String.format(
                    "%02d:%02d",
                    *arrayOf<Any>(Integer.valueOf(0), Integer.valueOf(1))
                )
                Log.d(
                    "myaccess",
                    "after calculation - $zeroSeconds --- $firstSecond --- $duration"
                )
                val parser = SimpleDateFormat("HH:mm")
                var ten: Date? = null
                try {
                    ten = parser.parse("00:00")
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                try {
                    val userDate = parser.parse(duration)
                    if (userDate.after(ten)) {
                        Toast.makeText(
                            applicationContext,
                            "call duration: $duration seconds", Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: ParseException) {
                    // Invalid date was entered
                }
                if (zeroSeconds == duration || firstSecond == duration) {
                    Toast.makeText(applicationContext, "Call answered", Toast.LENGTH_SHORT).show()
                }
                info.recycle()
            }
        }
    }

    override fun onInterrupt() {
        Log.e(TAG, "onInterrupt: something went wrong")
    }

    override fun onServiceConnected() {
        super.onServiceConnected()
        val info = AccessibilityServiceInfo()

        /*   info.eventTypes = AccessibilityEvent.TYPE_VIEW_CLICKED |
                AccessibilityEvent.TYPE_VIEW_FOCUSED;*/info.eventTypes =
            AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC
        info.notificationTimeout = 0


/*        info.packageNames = new String[]
                {"com.example.android.myFirstApp", "com.example.android.mySecondApp"};*/

        // Set the type of feedback your service will provide.
        // info.feedbackType = AccessibilityServiceInfo.FEEDBACK_SPOKEN;
        this.serviceInfo = info


        //executor to check whether the activity is running if not stop the service
        val scheduleTaskExecutor = Executors.newScheduledThreadPool(1)
        scheduleTaskExecutor.scheduleAtFixedRate({ serviceChecker() }, 0, 5, TimeUnit.SECONDS)
    }

    private fun serviceChecker() {
        if (!isActivityRunning(MainActivity::class.java)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //calling disable self stops the accessibility service
                disableSelf()
            }
        }
    }

    protected fun isActivityRunning(activityClass: Class<*>): Boolean {
        val activityManager = baseContext.getSystemService(ACTIVITY_SERVICE) as ActivityManager
        val tasks = activityManager.getRunningTasks(Int.MAX_VALUE)
        for (task in tasks) {
            if (activityClass.canonicalName.equals(
                    task.baseActivity!!.className,
                    ignoreCase = true
                )
            ) return true
        }
        return false
    }

    companion object {
        private const val TAG = "MyAccessibilityService"
    }
}